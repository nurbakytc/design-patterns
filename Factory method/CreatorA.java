public class CreatorA extends Creator{
    @Override
    Product createProduct() {
        return new ProductA();
    }
}
