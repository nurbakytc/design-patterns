public class Director {
    public void constructHouse(Builder builder){
        builder.buildDoors(4);
        builder.buildRoof(true);
        builder.buildGarage(true);
        builder.buildWalls(6);
        builder.buildWindows(8);
    }
}
