public class House {
    private final int Walls;
    private final int Doors;
    private final int Windows;
    private final boolean Roof;
    private final boolean Garage;

    public House(int walls, int doors, int windows, boolean roof, boolean garage) {
        Walls = walls;
        Doors = doors;
        Windows = windows;
        Roof = roof;
        Garage = garage;
    }
}
