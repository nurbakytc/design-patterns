public interface Builder {
    void buildWalls(int Walls);
    void buildDoors(int Doors);
    void buildWindows(int Windows);
    void buildRoof(boolean Roof);
    void buildGarage(boolean Garage);
}
