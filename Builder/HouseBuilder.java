public class HouseBuilder implements Builder{
    private int Walls;
    private int Doors;
    private int Windows;
    private boolean Roof;
    private boolean Garage;

    @Override
    public void buildWalls(int Walls) {
        this.Walls = Walls;
    }

    @Override
    public void buildDoors(int Doors) {
        this.Doors = Doors;
    }

    @Override
    public void buildWindows(int Windows) {
        this.Windows = Windows;
    }

    @Override
    public void buildRoof(boolean Roof) {
        this.Roof = Roof;
    }

    @Override
    public void buildGarage(boolean Garage) {
        this.Garage = Garage;
    }

    public House getResult(){
        return new House(Walls,Doors,Windows,Roof,Garage);
    }
}
